from django.db import models


class Blog(models.Model):
    title = models.CharField(max_length=100)
    slug = models.SlugField(unique=True, null=True, blank=True)
    image = models.ImageField(upload_to='blogs')
    content = models.TextField()
    author = models.CharField(max_length=100)
    posted_date = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.posted_date)


class Student(models.Model):
    name = models.CharField(max_length=100)
    grade = models.PositiveIntegerField()
    roll_no = models.PositiveIntegerField()
    image = models.ImageField(upload_to="students")
    fathers_name = models.CharField(max_length=100)
    mobile = models.CharField(max_length=100)

    def __str__(self):
        return self.name
