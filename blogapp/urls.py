from django.urls import path
from .views import *


app_name = 'blogapp'
urlpatterns = [
    path('', HomeView.as_view(), name='home'),
    path('contact/', ContactView.as_view(), name='contact'),
    path('about/', AboutView.as_view(), name='about'),
    path('blog/list/', BlogListView.as_view(), name='bloglist'),
    path('blog/<int:pk>/detail/', BlogDetailView.as_view(), name='blogdetail'),
    path('blog/<int:pk>/delete/', BlogDeleteView.as_view(), name='blogdelete'),
    path('blog/create/', BlogCreateView.as_view(), name='blogcreate'),



    path('blog/<int:pk>/update/', BlogUpdateView.as_view(), name='blogupdate'),



    path('student/list/', StudentListView.as_view(), name="studentlist"),



]
