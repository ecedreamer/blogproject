from django.views.generic import *
from .forms import *


class HomeView(TemplateView):
    template_name = 'home.html'


class ContactView(TemplateView):
    template_name = 'contact.html'


class AboutView(TemplateView):
    template_name = 'about.html'


class BlogListView(ListView):
    template_name = 'bloglist.html'
    queryset = Blog.objects.all().order_by('-id')
    context_object_name = 'bloglist'


class BlogDetailView(DetailView):
    template_name = 'blogdetail.html'
    model = Blog
    context_object_name = 'blogdetail'


class BlogCreateView(CreateView):
    template_name = 'blogcreate.html'
    form_class = BlogForm
    success_url = '/blog/list/'


class BlogUpdateView(UpdateView):
    template_name = 'blogcreate.html'
    model = Blog
    form_class = BlogForm
    success_url = '/blog/list/'


class BlogDeleteView(DeleteView):
    template_name = 'blogdelete.html'
    model = Blog
    success_url = '/blog/list/'


class StudentListView(ListView):
    template_name = "studentlist.html"
    queryset = Student.objects.all().order_by('-id')
    context_object_name = "all_students"
